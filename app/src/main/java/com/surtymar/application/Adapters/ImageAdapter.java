package com.surtymar.application.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.surtymar.application.Activities.MainActivity;
import com.surtymar.application.R;

import java.util.List;

/**
 * Created by nizar on 15/07/16.
 */
public class ImageAdapter extends BaseAdapter {

    private Context context;
    private List<Bitmap> bitmapList ;
    private int layoutResourceId;

    public ImageAdapter(Context context, List<Bitmap> bitmapList, int layoutResourceId) {
        this.context = context;
        this.bitmapList = bitmapList;
        this.layoutResourceId = layoutResourceId;
    }

    @Override
    public int getCount() {
        return bitmapList.size();
    }

    @Override
    public Object getItem(int i) {
        return bitmapList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View row = view;
        ViewHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((MainActivity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, viewGroup, false);
            holder = new ViewHolder();
             holder.image = (ImageView) row.findViewById(R.id.image);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }


        holder.image.setImageBitmap(bitmapList.get(i));
        return row;
    }

    static class ViewHolder {

        ImageView image;
    }
}
