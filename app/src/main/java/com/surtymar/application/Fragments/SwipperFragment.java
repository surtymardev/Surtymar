package com.surtymar.application.Fragments;

import android.R.style;
import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.surtymar.application.Beans.Parameters;
import com.surtymar.application.R;
import com.surtymar.application.Activities.MainActivity;
import com.surtymar.application.Beans.Category;
import com.surtymar.application.Beans.Child_pages;
import com.surtymar.application.Beans.ElementSwipe;
import com.surtymar.application.Beans.Illustration;
import com.surtymar.application.Beans.Parameters_swipe;
import com.surtymar.application.Beans.Section;

import com.surtymar.application.utils.Colors;
import com.surtymar.application.utils.Utils;
import com.surtymar.application.Widgets.ArrowImageView;
import com.surtymar.application.Widgets.AutoResizeTextView;


import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.realm.Realm;

public class SwipperFragment extends Fragment {

	private View view;
	private MyPageAdapter pageAdapter;
	private ViewPager mViewPager;

	private Colors colors;

	private boolean isTablet;
	private List<ElementSwipe> elementSwipes;
	private Parameters_swipe parameters_swipe;
    public Realm realm;

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig); 


	}


	@Override
	public void onAttach(Activity activity) {
		
	//new AppController(getActivity());
				realm = Realm.getInstance(getActivity());
		colors = ((MainActivity)activity).colors;
        Parameters ParamColor =realm.where(Parameters.class).findFirst();
        if (colors==null) {
            colors = new Colors(ParamColor);
        }

		((MainActivity)getActivity()).bodyFragment = "SwipperFragment";
		if(((MainActivity)getActivity()).extras == null)
			((MainActivity)getActivity()).extras = new Bundle();
		int section_id = -1;
		if (getArguments() != null && getArguments().getInt("Section_id")!=0) {
			section_id = getArguments().getInt("Section_id");
			((MainActivity)getActivity()).extras.putInt("Section_id", section_id);
			//id = section_id; 
		}
		else if (getArguments() != null && getArguments().getInt("Category_id") != 0)
		{
			int category_id = getArguments().getInt("Category_id");
		((MainActivity)getActivity()).extras.putInt("Category_id", category_id);
		//id = category_id;
		}
        isTablet = Utils.isTablet(activity);
		//time = System.currentTimeMillis();

		super.onAttach(activity);
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		setRetainInstance(true);

		elementSwipes = new ArrayList<ElementSwipe>();
        elementSwipes = realm.where(ElementSwipe.class).findAll();//appController.getElemantsDao().queryForAll();
        //Parameters_swipe parameters_swipe = null;
        parameters_swipe = realm.where(Parameters_swipe.class).findFirst();//appController.getParameters_swipeDao().queryForId(1);
        List<Fragment> fragments2 = getFragments(elementSwipes);
		//		Collections.reverse(fragments2);
		pageAdapter = new MyPageAdapter(getChildFragmentManager()/*getChildFragmentManager()*/, fragments2);


		super.onCreate(savedInstanceState);
	}
	//	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {

		try {
			mViewPager.setAdapter(pageAdapter);

			mViewPager.setCurrentItem(0);

			if (elementSwipes.size()>1) {
				pageSwitcher(5, elementSwipes.size());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = new View(getActivity());
		try {
			if (isTablet) {
				if (parameters_swipe.getLogo_position().equalsIgnoreCase("center_top")) {
					view = inflater.inflate(R.layout.swipper_center_top_layout, container, false);
				}else {
					view = inflater.inflate(R.layout.swipper_layout, container, false);
				}
			}else {
				view = inflater.inflate(R.layout.swipper_center_top_layout_smart, container, false);

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		buildSwipe();
		return view;
	}



	public void buildSwipe() {
		mViewPager = (ViewPager)view.findViewById(R.id.pager);

		if (parameters_swipe != null) {

			Illustration illustration = parameters_swipe.getIllustration();
			ImageView logo_swipe = (ImageView)view.findViewById(R.id.logo_swipe);
			if (illustration != null) {
				try {
					Glide.with(getActivity()).load(new File(illustration.getPath())).into(logo_swipe);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//				imageLoader.displayImage(path, logo_swipe);
			}else {
				try {
					Glide.with(getActivity()).load(parameters_swipe.getLogo()).into(logo_swipe);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//				imageLoader.displayImage(parameters_swipe.getLogo(), logo_swipe);
			}

			TextView title_swipe = (AutoResizeTextView)view.findViewById(R.id.swipe_title);
			/*ColorStateList colorStateList = new ColorStateList(
					new int[][] {new int[] { android.R.attr.state_pressed }, new int[] {} },
					new int[] {Color.GRAY, colors.getColor(parameters_swipe.getColor()) });*/
//			title_swipe.setTextColor(colorStateList);
			
			//title_swipe.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);

			title_swipe.setTextAppearance(getActivity(), style.TextAppearance_Large);
			title_swipe.setTypeface(MainActivity.FONT_TITLE, Typeface.BOLD);
            if(!parameters_swipe.getColor().isEmpty())
			title_swipe.setTextColor(colors.getColor(parameters_swipe.getColor()));
            else
                title_swipe.setTextColor(colors.getColor("FFFFFF"));
			title_swipe.setText(parameters_swipe.getButton_title());
			//			title_swipe.setTextSize(42);

			final ArrowImageView swipeAImg = (ArrowImageView)view.findViewById(R.id.title_arrow);
			Paint paint = new Paint();
//			paint.setColor(Color.WHITE);
            if(!parameters_swipe.getColor().isEmpty())
			paint.setColor(colors.getColor(parameters_swipe.getColor()));
            else
            paint.setColor(colors.getColor("FFFFFF"));
			swipeAImg.setLayoutParams(new LinearLayout.LayoutParams((int)title_swipe.getTextSize() - 6, (int)title_swipe.getTextSize()  - 6));
			swipeAImg.setPaint(paint);

			//title_swipe.setTextSize(38);
			//			RelativeLayout swipeBtn = (RelativeLayout) findViewById(R.id.swipe_btn);
			//			ImageView swipe_logo_holder = (ImageView)findViewById(R.id.logo_swipe);
			LinearLayout sub_swipe_title_holder = (LinearLayout)view.findViewById(R.id.sub_swipe_title_holder);
			//sub_swipe_title_holder.setPadding(0, 40, 0, 0);
			if (parameters_swipe.getSection_id()!= 0) {
				final int section_id = parameters_swipe.getSection_id();

				OnClickListener swipeClick = new OnClickListener() {

					@Override
					public void onClick(View v) {}
				};
				logo_swipe.setOnClickListener(swipeClick );
				sub_swipe_title_holder.setOnClickListener(swipeClick );
			}else if (parameters_swipe.getCategory_id()!= 0) {
				final int category_id = parameters_swipe.getCategory_id();
				OnClickListener swipeClick = new OnClickListener() {

					@Override
					public void onClick(View v) {
						List<Category> categories = new ArrayList<Category>();
                        categories =  realm.where(Category.class).equalTo("id", category_id).findAll();
                        if (categories.size()>0) {
//							((MainActivity)getActivity()).openCategory(categories.get(0));
							if (timer != null) {timer.cancel();}
						}



					}
				};
				logo_swipe.setOnClickListener(swipeClick );
				sub_swipe_title_holder.setOnClickListener(swipeClick );

			}else if (parameters_swipe.getPage_id()!= 0) {
				final int page_id = parameters_swipe.getPage_id();
				OnClickListener swipeClick = new OnClickListener() {

					@Override
					public void onClick(View v) {

						List<Child_pages> pages = new ArrayList<Child_pages>();
                        pages = realm.where(Child_pages.class).equalTo("id", page_id).findAll();


						if (timer != null) {timer.cancel(); timer.purge();}

					}
				};
				logo_swipe.setOnClickListener(swipeClick );
				sub_swipe_title_holder.setOnClickListener(swipeClick );

			}
			StateListDrawable stateDrawable = new StateListDrawable();
			stateDrawable.addState(new int[]{android.R.attr.state_pressed}, new ColorDrawable(Color.parseColor("#88ffffff")));
			stateDrawable.addState(new int[]{android.R.attr.state_focused}, new ColorDrawable(Color.parseColor("#88ffffff"))); 
			if (isTablet) {
				stateDrawable.addState(new int[]{}, new ColorDrawable(Color.TRANSPARENT)); 
			}else {
				stateDrawable.addState(new int[]{}, new ColorDrawable(Color.parseColor("#AA000000"))); 
			}

			sub_swipe_title_holder.setBackgroundDrawable(stateDrawable);

			swipeAImg.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					ArrowImageView img = (ArrowImageView) v;
					Paint paint = new Paint();
					paint.setColor(Color.GRAY);
					img.setPaint(paint);
					return false;
				}
			});

		}

	}



	public Timer timer;
	int page = 0;

	public void pageSwitcher(int seconds, int max_page) {
		timer = new Timer(); // At this line a new Thread will be created
		timer.scheduleAtFixedRate(new RemindTask(max_page), 0, seconds * 1000); // delay
		// in
		// milliseconds
	}

	// this is an inner class...
	class RemindTask extends TimerTask {

		int maxPage;


		@Override
		public void run() {

			// As the TimerTask run on a separate thread from UI thread we have
			// to call runOnUiThread to do work on UI thread.
			if(((MainActivity)getActivity()) == null) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else 
			((MainActivity)getActivity()).runOnUiThread(new Runnable() {
				public void run() {

					if (page >= maxPage) { // In my case the number of pages are 5
						page = 0;
						mViewPager.setCurrentItem(page++);

					} else {
						mViewPager.setCurrentItem(page++);
					}

				}
				//				}
			});

		}


		public RemindTask(int maxPage) {
			super();
			this.maxPage = maxPage;
		}
	}

	private List<Fragment> getFragments(List<ElementSwipe> elementSwipes){
		List<Fragment> fList = new ArrayList<Fragment>();
		for (int i = 0; i < elementSwipes.size(); i++) {
			fList.add(SwipePage.newInstance(elementSwipes.get(i).getId()));
		}



		return fList;
	}

	private class MyPageAdapter extends FragmentPagerAdapter {
		private List<Fragment> fragments;

		public MyPageAdapter(FragmentManager fm, List<Fragment> fragments) {
			super(fm);
			this.fragments = fragments;
		}
		@Override
		public Fragment getItem(int position) {
			return this.fragments.get(position);
		}

		@Override
		public int getCount() {
			return this.fragments.size();
		}
	}


	@Override
	public void onPause() {
		if (timer!=null) {
			timer.cancel();
			//timer.purge();
		}
		super.onPause();
	}

	@Override
	public void onStop() {
		if (timer!=null) {
			Runtime.getRuntime().gc();
			timer.cancel();
			//timer.purge();
		}
		super.onStart();
	}

	@Override
	public void onDestroy(){
		Runtime.getRuntime().gc();
		if (timer!=null) {
			timer.cancel();
		}


		super.onDestroy();
	}

	@Override
	public void onResume() {

		super.onResume();
	}

}
