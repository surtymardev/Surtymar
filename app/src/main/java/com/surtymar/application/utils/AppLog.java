package com.surtymar.application.utils;

/**
 * Created by nizar on 19/07/16.
 */
import android.util.Log;

public class AppLog {
    private static final String APP_TAG = "AudioRecorder";

    public static int logString(String message) {
        return Log.e(APP_TAG, message);
    }
}
