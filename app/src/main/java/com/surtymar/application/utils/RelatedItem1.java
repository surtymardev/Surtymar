package com.surtymar.application.utils;

import com.surtymar.application.Beans.RelatedCatIds;
import com.surtymar.application.Beans.RelatedContactForm;
import com.surtymar.application.Beans.RelatedLocation;

import io.realm.RealmList;

/**
 * Created by Euphor on 30/04/2015.
 */
public interface  RelatedItem1 {
    public  String getItemTitle1();
    public  String getItemIntro1();
    public Object getItemIllustration1();
    public String getItemType1();
    public RealmList<RelatedCatIds> getRelatedCategories1();
    public int getItemExtra1();
    public RelatedContactForm getRelatedContactForm();
    public RelatedLocation getRelatedLocation();
}

