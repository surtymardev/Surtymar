package com.surtymar.application.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.surtymar.application.Beans.Category;
import com.surtymar.application.Beans.Parameters;
import com.surtymar.application.Beans.Section;
import com.surtymar.application.Beans.Tab;
import com.surtymar.application.Fragments.ContactsFragment;
import com.surtymar.application.Fragments.MapV2Fragment;
import com.surtymar.application.Fragments.SwipperFragment;
import com.surtymar.application.Fragments.TabsSupportFragment;
import com.surtymar.application.Fragments.WelcomeFragment;
import com.surtymar.application.MyApplication;
import com.surtymar.application.R;
import com.surtymar.application.Widgets.slidingMenu.SlidingMenu;
import com.surtymar.application.utils.Colors;
import com.surtymar.application.utils.Constants;
import com.surtymar.application.utils.Utils;
import com.surtymar.application.utils.InfoActivity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmList;

public class MainActivity extends FragmentActivity implements TabsSupportFragment.ActionCallBack {

    private TabsSupportFragment tabsFragment;
    public String bodyFragment;
    private SlidingMenu menu;
    private View shadow;
    public static short positionNav = -1;
    public static Typeface FONT_REGULAR;
    public static Typeface FONT_BOLD;
    public static Typeface FONT_TITLE;
    public static Typeface FONT_ITALIC;
    public static Typeface FONT_BODY;
    public Colors colors;
    public static InfoActivity infoActivity;
    public Bundle extras;
    private boolean isTablet;
    private Realm realm;
    public String regid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        isTablet = Utils.isTablet(getApplicationContext());
        infoActivity = new InfoActivity(0, 0, "");
        realm = Realm.getInstance(getApplicationContext());
        Parameters param = realm.where(Parameters.class).findFirst();
        colors = new Colors(param);

        if (isTablet) {
            // setContentView(R.layout.top_frame_container);
            setContentView(R.layout.frame_container);
            //       setContentView(R.layout.bottom_frame_container);
        }


        Map<String, Object> map = (HashMap<String, Object>) getLastCustomNonConfigurationInstance();
        String type = null;
        if (map == null || map.isEmpty()) {

            Bundle bundle = getIntent().getExtras();
            TabsSupportFragment tabsFragment = new TabsSupportFragment();
            if (bundle != null) {
                type = bundle.getString("extra");
                if (bundle.getParcelable("InfoActivity") != null) {
                    infoActivity = bundle.getParcelable("InfoActivity");
                    Bundle extrasTabsFrag = new Bundle();
                    extrasTabsFrag.putInt("highlighted_tab", infoActivity.getIdCurrentTab());
                    extrasTabsFrag.putBoolean("orientation", true);
                    tabsFragment.setArguments(extrasTabsFrag);
                }
            }
            ((MyApplication) getApplication()).setFragment(MainActivity.this, tabsFragment, R.id.tabsFragment);


            //   if (type != null) {
            extras = getIntent().getExtras();
            //findViewById(R.id.fragment_container).setVisibility(View.VISIBLE);
          //  findViewById(R.id.swipe_container).setVisibility(View.VISIBLE);

            Log.e(" swipperFragment :", " Yes it is " + " ");
            SwipperFragment swipperFragment = new SwipperFragment();
            bodyFragment = "SwipperFragment";
            swipperFragment.setArguments(extras);
           ((MyApplication) getApplication()).setFragment(MainActivity.this, swipperFragment, R.id.fragment_container);

            //  WelcomeFragment welcomeFragment = new WelcomeFragment();
            //((MyApplication) getApplication()).setFragment(MainActivity.this, welcomeFragment, R.id.fragment_container);

          /*  ContactsFragment contactsFragment = new ContactsFragment();
            extras = (Bundle) map.get("EXTRAS");
            bodyFragment = "ContactsFragment";
            contactsFragment.setArguments(extras);
            ((MyApplication) getApplication()).setFragment(MainActivity.this, contactsFragment, R.id.fragment_container);
          */
        }else {
            tabsFragment = new TabsSupportFragment();
            Bundle extrasTabsFrag = new Bundle();
            if (map.get("SelectedTab") != null) {
                int index = (Integer) map.get("SelectedTab");
                extrasTabsFrag.putInt("highlighted_tab", index);
                extrasTabsFrag.putBoolean("orientation", true);
                if (infoActivity.getIdCurrentTab() != Constants.DEFAULT_TAB_VALUE) {
                    infoActivity.switchCurrentPrev();
                    infoActivity.setIdCurrentTab(index);
                } else {
                    infoActivity.setIdCurrentTab(index);
                }

            } else {
                extrasTabsFrag.putInt("highlighted_tab", Constants.DEFAULT_TAB_VALUE);
                extrasTabsFrag.putBoolean("orientation", false);
                infoActivity.setIdCurrentTab(Constants.DEFAULT_TAB_VALUE);
            }
            type = ((String) map.get("body"));
            if (type != null) {
                if (type.equals("ContactsFragment")) {
                    ContactsFragment contactsFragment = new ContactsFragment();
                    extras = (Bundle) map.get("EXTRAS");
                    contactsFragment.setArguments(extras);
                    bodyFragment = "ContactsFragment";

                    MainActivity.this.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,contactsFragment).addToBackStack(null).commit();
                }

            }
        }

        menu = new SlidingMenu(MainActivity.this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
        menu.attachToActivity(MainActivity.this, SlidingMenu.SLIDING_WINDOW);
        //menu.setShadowWidth(80);
        //menu.setShadowDrawable(R.drawable.background_shadow);
        menu.setBehindOffset(80);
        menu.setFadeDegree(0.35f);
        menu.setMenu(R.layout.selection_favorites_view);
        //menu.setBackgroundColor(Utils.getBackgroundColor());
        if (isTablet)
            menu.setBehindWidthRes(R.dimen.menu_width_res);
        else
            menu.setBehindWidthRes(R.dimen.menu_width_res_phone);

        menu.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        menu.setSlidingEnabled(true);
        menu.setTouchmodeMarginThreshold(2);

        if (!isTablet) {
            ImageView Homebutton = (ImageView) findViewById(R.id.menu_home_button);
            ImageView Back = (ImageView) findViewById(R.id.back_arrow);
            LinearLayout close = (LinearLayout) findViewById(R.id.closeFavoritesContainer);
            shadow = findViewById(R.id.shadowView);

            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CloseMenu();
                }
            });

            Homebutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    OpenMenu();
                }
            });
            shadow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CloseMenu();
                }
            });
            Back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getSupportFragmentManager().popBackStack();
                }
            });


        }

    }

    @Override
    public Map<String, Object> onRetainCustomNonConfigurationInstance() {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("SelectedTab", tabsFragment.indexSeletedTab);
        map.put("body", bodyFragment);
        map.put("EXTRAS", extras);
        return map;
    }


    public void CloseMenu() {
        shadow.setVisibility(View.GONE);
        menu.toggle(true);
    }

    public void OpenMenu() {
        menu.showMenu(true);
        shadow.setVisibility(View.VISIBLE);
    }


    @Override
    public void onTabClick(Tab tab, int index) {
        infoActivity.switchCurrentPrev();
        infoActivity.setIdCurrentTab(index);


        Section section = null;
        realm = Realm.getInstance(getApplicationContext());
        List<Section> sections = realm.where(Section.class).equalTo("id", tab.getSection_id()).findAll();
        if (sections.size() > 0) {
            section = sections.get(0);
        }
        if (section != null) {

            openSection(tab, section);


        }

    }


    /**
     * @param tab
     * @param section
     */
    public void openSection(Tab tab, Section section) {
        String type = section.getType();
        extras = new Bundle();
        if (type.equals("contents") || type.isEmpty()) { // type Contents or
            if (!tab.getIsHomeGrid()) {


            } else if (tab.getIsHomeGrid()) {


            } else if (section.getDisplay_type() != null && section.getDisplay_type().equals("column_scroll")) {
            }
        } else if (type.equals("agenda")) {


        } else if (type.equals("interactive_map")) {
            if (section.getMaps() != null) {
            }
        } else if (type.equals("map")) {
            MapV2Fragment mMapFragment = new MapV2Fragment();
            bodyFragment = "MapV2Fragment";
            FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, mMapFragment);
            fragmentTransaction.addToBackStack("MapV2Fragment").commit();

        } else if (type.equals("contact")) { // Type Contacts

            ContactsFragment contactsFragment = new ContactsFragment();
            bodyFragment = "ContactsFragment";
            extras.putInt("Section_id", section.getId_s());
            contactsFragment.setArguments(extras);
            // Add the fragment to the 'fragment_container' FrameLayout
            MainActivity.this
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, contactsFragment)
                    .addToBackStack(null).commit();

            findViewById(R.id.fragment_container).setVisibility(View.VISIBLE);
            findViewById(R.id.swipe_container).setVisibility(View.GONE);


        }
        else
        {
            SwipperFragment swipperFragment = new SwipperFragment();
            bodyFragment = "SwipperFragment";
            extras.putInt("Section_id", section.getId_s());
            swipperFragment.setArguments(extras);
            // Add the fragment to the 'fragment_container' FrameLayout
            MainActivity.this
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, swipperFragment)
                    .addToBackStack(null).commit();

            findViewById(R.id.fragment_container).setVisibility(View.VISIBLE);
            findViewById(R.id.swipe_container).setVisibility(View.GONE);
        }
    }

    private LocationManager mLocationManager;
    private Location lastLocation;

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            lastLocation = location;
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling

                return;
            }
            mLocationManager.removeUpdates(mLocationListener);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    public void openCategory(Category category) {



        if (category != null) {

            if(category.getParameters().contains("geolock")){

                if (lastLocation != null) {
                    String[] spliter = category.getParameters().split("[geolock\\,\\[\\]]+");
                    double lat, lon, rayon;
                    lat = lon = rayon = 0;

                    if(spliter.length == 4){
                        lat = Double.parseDouble(spliter[1]);
                        lon = Double.parseDouble(spliter[2]);
                        rayon = Double.parseDouble(spliter[3]);
                    }
                    Location geoLocklocation = new Location("geoLock");
                    geoLocklocation.setLatitude(lat);
                    geoLocklocation.setLongitude(lon);

                    if(lastLocation.distanceTo(geoLocklocation) > rayon){
                        //Toast.makeText(getApplicationContext(), getString(R.string.geoLockMsg), Toast.LENGTH_LONG).show();
                        AlertDialog alertDialog = new AlertDialog.Builder(this).create();

                        // Setting Dialog Message
                        alertDialog.setMessage(getString(R.string.geoLockMsg));
                        // Setting alert dialog icon
                        alertDialog.setIcon(android.R.drawable.ic_dialog_info);

                        // Setting OK Button
                        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                        // Showing Alert Message
                        alertDialog.show();
                        return;
                    }
                    //locationManager.onLocationChanged(lastLocation);
                }
            }
            if(category.getLocation_group_id()>0 && !(""+category.getLocation_group_id()).isEmpty()){

            }


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
