package com.surtymar.application.Activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.android.volley.toolbox.JsonObjectRequest;
import com.surtymar.application.Beans.AgendaGroup;
import com.surtymar.application.Beans.Album;
import com.surtymar.application.Beans.AllowedDay;
import com.surtymar.application.Beans.Allowed_period_day;
import com.surtymar.application.Beans.Allowed_period_weekdays;
import com.surtymar.application.Beans.Application;
import com.surtymar.application.Beans.ApplicationBean;
import com.surtymar.application.Beans.Cart;
import com.surtymar.application.Beans.CategoriesMyBox;
import com.surtymar.application.Beans.Category;
import com.surtymar.application.Beans.Center_location;
import com.surtymar.application.Beans.Child_pages;
import com.surtymar.application.Beans.CoffretID;
import com.surtymar.application.Beans.Contact;
import com.surtymar.application.Beans.Coordinates;
import com.surtymar.application.Beans.Disable_period;
import com.surtymar.application.Beans.Disallowed_period_everyday;
import com.surtymar.application.Beans.ElementSwipe;
import com.surtymar.application.Beans.Event;
import com.surtymar.application.Beans.ExtraField;
import com.surtymar.application.Beans.Extra_fields;
import com.surtymar.application.Beans.Field;
import com.surtymar.application.Beans.FieldFormContact;
import com.surtymar.application.Beans.FormValue;
import com.surtymar.application.Beans.Formule;
import com.surtymar.application.Beans.FormuleElement;
import com.surtymar.application.Beans.Illustration;
import com.surtymar.application.Beans.Link;
import com.surtymar.application.Beans.Linked;
import com.surtymar.application.Beans.Location;
import com.surtymar.application.Beans.Locations_group;
import com.surtymar.application.Beans.MyArrayList;
import com.surtymar.application.Beans.MyInteger;
import com.surtymar.application.Beans.MyString;
import com.surtymar.application.Beans.Parameters;
import com.surtymar.application.Beans.Parameters_;
import com.surtymar.application.Beans.Parameters__;
import com.surtymar.application.Beans.Parameters_section;
import com.surtymar.application.Beans.Parameters_swipe;
import com.surtymar.application.Beans.PeriodString;
import com.surtymar.application.Beans.Phone_home_grid;
import com.surtymar.application.Beans.Photo;
import com.surtymar.application.Beans.Question;
import com.surtymar.application.Beans.Radio;
import com.surtymar.application.Beans.Related;
import com.surtymar.application.Beans.RelatedCatIds;
import com.surtymar.application.Beans.RelatedContactForm;
import com.surtymar.application.Beans.RelatedLocation;
import com.surtymar.application.Beans.RelatedPageId;
import com.surtymar.application.Beans.Score;
import com.surtymar.application.Beans.Section;
import com.surtymar.application.Beans.Street_view_default_position;
import com.surtymar.application.Beans.StringImagesBox;
import com.surtymar.application.Beans.StringScore;
import com.surtymar.application.Beans.StringValidityBox;
import com.surtymar.application.Beans.Survey_;
import com.surtymar.application.Beans.Tab;
import com.surtymar.application.Beans.Tablet_home_grid;
import com.surtymar.application.Beans.Tile;
import com.surtymar.application.Beans.Tile_;
import com.surtymar.application.Beans.Value;
import com.surtymar.application.Beans.ValueSatisfaction;
import com.surtymar.application.R;
import com.surtymar.application.utils.Utils;
import com.surtymar.application.utils.Utils1;

import java.io.IOException;
import java.io.InputStream;

import io.realm.Realm;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class SplashActivity extends FragmentActivity {


    public static com.android.volley.RequestQueue mRequestQueue;
    Realm realm;
    private parsejson pj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Utils1.activity = this;



        mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        String url = "http://backoffice.paperpad.fr/api/application/compiled_app/id/28/lang/fr";
        JsonObjectRequest request = new JsonObjectRequest(url,  new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {


                if (response != null) {
                    if (pj != null) {
                        pj.cancel(true);
                    }

                    pj = new parsejson(response);
                    pj.execute();
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }
        );

        request.setRetryPolicy(new DefaultRetryPolicy(
                9000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);





    }
         class parsejson extends AsyncTask<Void,Void, Void> {
             JSONObject jsonObject;

             public parsejson(JSONObject jsonObject) {
                 this.jsonObject = jsonObject;
             }

             @Override
             protected void onPreExecute() {
                 super.onPreExecute();
                 Realm r = Realm.getInstance(getApplicationContext());
                 r.beginTransaction();
                 long time = System.currentTimeMillis();
                 r.where(ApplicationBean.class).findAll().clear();
                 r.where(AgendaGroup.class).findAll().clear();
                 r.where(Album.class).findAll().clear();
                 r.where(Allowed_period_day.class).findAll().clear();
                 r.where(Allowed_period_weekdays.class).findAll().clear();
                 r.where(AllowedDay.class).findAll().clear();
                 r.where(Application.class).findAll().clear();
                 r.where(Cart.class).findAll().clear();
                 r.where(CategoriesMyBox.class).findAll().clear();
                 r.where(Category.class).findAll().clear();
                 r.where(Center_location.class).findAll().clear();
                 r.where(Child_pages.class).findAll().clear();
                 r.where(CoffretID.class).findAll().clear();
                 r.where(Contact.class).findAll().clear();
                 r.where(Coordinates.class).findAll().clear();
                 r.where(Disable_period.class).findAll().clear();
                 r.where(Disallowed_period_everyday.class).findAll().clear();
                 r.where(ElementSwipe.class).findAll().clear();
                 r.where(Event.class).findAll().clear();
                 r.where(Extra_fields.class).findAll().clear();
                 r.where(ExtraField.class).findAll().clear();
                 r.where(Field.class).findAll().clear();
                 r.where(FieldFormContact.class).findAll().clear();
                 r.where(FormValue.class).findAll().clear();
                 r.where(Formule.class).findAll().clear();
                 r.where(FormuleElement.class).findAll().clear();
                 r.where(Link.class).findAll().clear();
                 r.where(Linked.class).findAll().clear();
                 r.where(Location.class).findAll().clear();
                 r.where(Locations_group.class).findAll().clear();
                 r.where(MyArrayList.class).findAll().clear();
                 r.where(MyInteger.class).findAll().clear();
                 r.where(MyString.class).findAll().clear();
                 r.where(Parameters.class).findAll().clear();
                 r.where(Parameters_.class).findAll().clear();
                 r.where(Parameters__.class).findAll().clear();
                 r.where(Parameters_section.class).findAll().clear();
                 r.where(Parameters_swipe.class).findAll().clear();
                 r.where(PeriodString.class).findAll().clear();
                 r.where(Illustration.class).findAll().clear();
                 r.where(Phone_home_grid.class).findAll().clear();
                 r.where(Photo.class).findAll().clear();
                 r.where(Question.class).findAll().clear();
                 r.where(Radio.class).findAll().clear();
                 r.where(Related.class).findAll().clear();
                 r.where(RelatedCatIds.class).findAll().clear();
                 r.where(RelatedContactForm.class).findAll().clear();
                 r.where(RelatedLocation.class).findAll().clear();
                 r.where(RelatedPageId.class).findAll().clear();
                 r.where(Score.class).findAll().clear();
                 r.where(Section.class).findAll().clear();
                 r.where(Street_view_default_position.class).findAll().clear();
                 r.where(StringImagesBox.class).findAll().clear();
                 r.where(StringScore.class).findAll().clear();
                 r.where(StringValidityBox.class).findAll().clear();
                 r.where(Survey_.class).findAll().clear();
                 r.where(Tab.class).findAll().clear();
                 r.where(Tablet_home_grid.class).findAll().clear();
                 r.where(Tile.class).findAll().clear();
                 r.where(Tile_.class).findAll().clear();
                 r.where(Value.class).findAll().clear();
                 r.where(ValueSatisfaction.class).findAll().clear();
                 r.removeAllChangeListeners();
                 time = System.currentTimeMillis() - time;
                 Log.e("le temp de pour vider la base :" + time / 1000, " (s)");
                 r.commitTransaction();
                 Log.e("size application :" + r.where(ApplicationBean.class).findAll().size(), "");
             }

             @Override
             protected Void doInBackground(Void ... objects) {
                 realm = Realm.getInstance(getApplicationContext());

                 realm.beginTransaction();


                 try {
                     InputStream stream = getAssets().open("data.json");
                    // realm.createOrUpdateObjectFromJson(ApplicationBean.class, jsonObject);
                     realm.createOrUpdateObjectFromJson(ApplicationBean.class, Utils.retreiveJsonFromAssetsFile("data.json", getApplicationContext()));
                     realm.commitTransaction();

                 } catch (IOException e) {
                     realm.cancelTransaction();
                     e.printStackTrace();
                 }
               return null;
             }

             @Override
             protected void onPostExecute(Void aVoid) {
                 super.onPostExecute(aVoid);
                 Intent i = new Intent(getApplicationContext(), MainActivity.class);
                 startActivity(i);
                 finish();
             }
         }
         }

